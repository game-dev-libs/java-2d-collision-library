This library is ment to provide a framework for collisions in 2d games.  
  
All worlds provided by this library will not run contain any physics, meaning you have to code them on your own.  
  
All world types allow changing the underlying shape of any objects in the world at any time.
package world2d;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.gdxutility.textures.TextureManager;
import com.felixcool98.launchutils.lwjgl3.LaunchUtilsLwjgl3;
import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.World2DActor;
import com.felixcool98.worlds.World2DRendererDrawable;
import com.felixcool98.worlds.bruteforce.BruteForceWorld;
import com.felixcool98.worlds.objects.World2DObjectData;

public class DrawTest extends ApplicationAdapter {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setTitle("DrawTest");
		config.setWindowSizeLimits(700, 500, 700, 500);
		
		LaunchUtilsLwjgl3.launch(new DrawTest(), config);
	}
	
	
	private World2DRendererDrawable test;
	private Batch batch;
	private Stage stage;
	
	
	@Override
	public void create() {
		super.create();
		
		stage = new Stage();
		
		
		TextureRegion fields = TextureManager.INSTANCE.getTextureRegion("com/felixcool98/tests/res/yellow.png");
		
		World2D world = new BruteForceWorld();
		
		test = new World2DRendererDrawable();
		test.setPosition(-1, -1);
		test.setSize(10, 10);
		test.setWorld(world);
		
		world.create(new World2DObjectData(new Rectangle(-1, -1, 3, 3), new TextureRegionDrawable(fields)));
		
		for(int i = 0; i < 14; i++) {
			world.create(new World2DObjectData(new Rectangle(i, i, 2, 2), new TextureRegionDrawable(fields)));
		}
		
		batch = new SpriteBatch();
		
		World2DActor actor = new World2DActor();
		
		actor.setWorld(world);
		actor.setSize(100, 100);
		actor.setViewSize(10, 10);
		
		stage.addActor(actor);
		
//		Image image = new Image(lake);
//		image.setSize(100, 100);
//		image.setPosition(100, 0);
//		getStage().addActor(image);
	}
	
	@Override
	public void render() {
		GdxUtils.clearScreen(Color.BLACK);
		
		batch.begin();
			test.draw(batch, 100, 100, 100, 100);
			test.drawWithoutScissors(batch, 200, 100, 100, 100);
		batch.end();
		
		ShapeRenderer renderer = new ShapeRenderer();
		renderer.setAutoShapeType(true);
		renderer.begin();
			renderer.rect(100, 100, 100, 100);
		renderer.end();
		
		if(Gdx.input.isKeyPressed(Keys.D))
			test.addPosition(0.1f, 0);
		
		stage.draw();
		stage.act();
	}
}

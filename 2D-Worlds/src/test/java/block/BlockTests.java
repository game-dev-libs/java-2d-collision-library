package block;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.aabb.shapes.RelativeLine;
import com.felixcool98.worlds.blocks.BlockWorld;

public class BlockTests {
	@Test
	public void test() {
		BlockWorld world = new BlockWorld();
		world.create(new Rectangle(0, 3, 1, 1));
		world.create(new Rectangle(1, 3, 1, 1));
		
		assertTrue(world.getIntersections(new Rectangle(0, 4, 2, 2)).getIntersections().isEmpty());
		assertTrue(world.getIntersections(new Rectangle(0, 4, 2, 2)).getIntersections().isEmpty());
		assertTrue(world.getIntersections(new RelativeLine(0, 4, 2, 0)).getIntersections().isEmpty());
		assertEquals(2, world.getIntersections(new RelativeLine(0, 3, 2, 0)).getIntersections().size());
	}
}

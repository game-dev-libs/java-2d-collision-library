package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import block.BlockTests;
import brute.BruteForceWorldTests;
import grid.GridTests;

@RunWith(Suite.class)
@SuiteClasses({BlockTests.class, BruteForceWorldTests.class, GridTests.class})
public class AllTests {

}

package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.aabb.shapes.RelativeLine;
import com.felixcool98.utility.values.Value;
import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.objects.World2DObject;
import com.felixcool98.worlds.objects.World2DObjectData;
import com.felixcool98.worlds.objects.World2DObject.World2DObjectChangeListener;

public abstract class WorldTest {
	public abstract World2D createEmptyWorld();
	
	
	public void runTests() {
		testCreate();
		testDestroy();
		testListeners();
		testIntersections();
		testEmpty();
		testLine();
	}
	private void testCreate() {
		World2D world = createEmptyWorld();
		
		World2DObjectData data = new World2DObjectData(new Rectangle(0, 0, 1, 1));
		
		World2DObject object = world.create(data);
		assertFalse(object.isDestroyed());
		
		data =  new World2DObjectData(new Rectangle(0, 1, 1, 1));
		
		world.create(data);
		
		assertEquals(2, world.size());
	}
	private void testDestroy() {
		World2D world = createEmptyWorld();
		
		World2DObjectData data = new World2DObjectData(new Rectangle(0, 0, 1, 1));
		
		World2DObject object = world.create(data);
		
		data = new World2DObjectData(new Rectangle(0, 1, 1, 1));
		
		world.create(data);
		
		object.destroy();
		
		assertEquals(1, world.size());
		
		world.destroyAll();
		
		assertEquals(0, world.size());
	}
	private void testListeners() {
		World2D world = createEmptyWorld();
		
		World2DObject obj = world.create(new Rectangle(1, 1));
		
		Value<Boolean> reached = new Value<>(false);
		
		obj.addListener(new World2DObjectChangeListener() {
			@Override
			public void changed(World2DObject object) {
				reached.set(true);
			}
		});
		
		obj.getShape().move(1, 1);
		
		assertTrue(reached.get());
		
		reached.set(false);
		
		obj.setShape(obj.getShape().clone());
		
		assertFalse(reached.get());
	}
	
	private void testIntersections() {
		testIntersectionsTouchingOutside();
		testInside();
		testIntersectionsTouchingInside();
	}
	private void testIntersectionsTouchingOutside() {
		World2D world = createEmptyWorld();
		
		new IntersectionsTouchingOutside(world).test();
	}
	private void testInside() {
		World2D world = createEmptyWorld();
		
		World2DObjectData data = new World2DObjectData(new Rectangle(0, 0, 1, 1));
		
		World2DObject object = world.create(data);
		
		assertEquals(1, world.size());
		
		Rectangle area = new Rectangle(0, 0, 10, 20);
		
		Rectangle shape = (Rectangle) object.getShape();
		
		shape.setPosition(1, 1);
		
		assertEquals(1, world.getIntersections(area).size());
	}
	private void testIntersectionsTouchingInside() {
		World2D world = createEmptyWorld();
		
		World2DObjectData data = new World2DObjectData(new Rectangle(0, 0, 1, 1));
		
		World2DObject object = world.create(data);
		
		assertEquals(1, world.size());
		
		Rectangle area = new Rectangle(0, 0, 10, 20);
		
		Rectangle shape = (Rectangle) object.getShape();
		
		shape.setBot(area.getBot());
		
		//left right
		shape.setLeft(area.getLeft());
		
		assertEquals(1, world.getIntersections(area).size());
		
		shape.setRight(area.getRight());
		
		assertEquals(1, world.getIntersections(area).size());
		
		//bot top
		shape.setLeft(area.getLeft());
		
		shape.setTop(area.getTop());
		
		assertEquals(1, world.getIntersections(area).size());
		
		shape.setBot(area.getBot());
		
		assertEquals(1, world.getIntersections(area).size());
	}
	
	private void testEmpty() {
		World2D world = createEmptyWorld();
		
		World2DObjectData data = new World2DObjectData(new Rectangle(0, 0, 1, 1));
		
		World2DObject object = world.create(data);
		
		assertEquals(1, world.size());
		assertFalse(world.isEmpty());
		
		object.destroy();
		object.destroy();
		
		assertEquals(0, world.size());
		assertTrue(world.isEmpty());
	}
	
	private void testLine() {
		World2D world = createEmptyWorld();
		
		world.create(new Rectangle(0, 0, 1, 1));
		
		assertEquals(1, world.getIntersections(new RelativeLine(0, 0, 1, 0)).size());
		assertEquals(1, world.getIntersections(new RelativeLine(0, 0, 0, 1)).size());
		assertEquals(1, world.getIntersections(new RelativeLine(0, 1, 1, 0)).size());
		assertEquals(1, world.getIntersections(new RelativeLine(1, 0, 0, 1)).size());
	}
}

package tests;

import static org.junit.Assert.assertEquals;

import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.objects.World2DObject;
import com.felixcool98.worlds.objects.World2DObjectData;

class IntersectionsTouchingOutside {
	private World2D world;
	
	private Rectangle area;
	private Rectangle shape;
	
	
	public IntersectionsTouchingOutside(World2D world) {
		this.world = world;
	}
	
	
	public void test() {
		World2DObjectData data = new World2DObjectData(new Rectangle(0, 0, 1, 1));
		
		World2DObject object = world.create(data);
		
		assertEquals(1, world.size());
		
		area = new Rectangle(0, 0, 10, 20);
		
		shape = (Rectangle) object.getShape();
		
//========================================================================
// horizontal
		
	//below bot
		shape.setTop(area.getBot());
		
		touchesLeft();
		touchesRight();
		
	//above bot
		shape.setBot(area.getBot());
		
		touchesLeft();
		touchesRight();
		
	//above top
		shape.setBot(area.getTop());
		
		touchesLeft();
		touchesRight();
		
	//below top
		shape.setTop(area.getTop());
		
		touchesLeft();
		touchesRight();
		
	//centered
		shape.setCenterY(area.getCenterY());
		
		touchesLeft();
		touchesRight();
		
//========================================================================
// vertical
		
	//right of right
		shape.setLeft(area.getRight());
		
		touchesTop();
		touchesBot();
		
	//left of right
		shape.setRight(area.getRight());
		
		touchesTop();
		touchesBot();
		
	//left of left
		shape.setRight(area.getLeft());
		
		touchesTop();
		touchesBot();
		
	//right of left
		shape.setLeft(area.getLeft());
		
		touchesTop();
		touchesBot();
		
	//centered
		shape.setCenterX(area.getCenterX());
		
		touchesTop();
		touchesBot();
	}
	
	private void touchesRight() {
		shape.setLeft(area.getRight());
		assertEquals(0, world.getIntersections(area).size());
	}
	private void touchesLeft() {
		shape.setRight(area.getLeft());
		assertEquals(0, world.getIntersections(area).size());
	}
	
	private void touchesTop() {
		shape.setBot(area.getTop());
		assertEquals(0, world.getIntersections(area).size());
	}
	private void touchesBot() {
		shape.setTop(area.getBot());
		assertEquals(0, world.getIntersections(area).size());
	}
}

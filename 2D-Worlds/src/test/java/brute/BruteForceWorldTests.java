package brute;

import org.junit.Test;

import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.bruteforce.BruteForceWorld;

import tests.WorldTest;

public class BruteForceWorldTests {
	@Test
	public void test() {
		new WorldTest() {
			@Override
			public World2D createEmptyWorld() {
				return new BruteForceWorld();
			}
		}.runTests();;
	}
}

package movementprocessor;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.grid.Grid;
import com.felixcool98.worlds.movement.MovementProcessor;
import com.felixcool98.worlds.objects.World2DObject;
import com.felixcool98.worlds.objects.World2DObjectData;

public class MovementProcessorTest {

	@Test
	public void test() {
		World2D world = new Grid();
		
		World2DObject object = world.create(new World2DObjectData(new Rectangle(1, 1)));
		
		world.create(new World2DObjectData(new Rectangle(1, 0, 1, 1)));
		
		new MovementProcessor().move(world, object, 2.1f, 0, new MovementProcessor.MovementProcessorCallback() {
			@Override
			public boolean hit(float x, float y, World2DObject origin, World2DObject object) {
				System.out.println("triggered");
				
				return false;
			}
		});
		
		System.out.println(object);
		
		assertEquals(2.1f, object.getShape().getAABB().getLeft(), 0.1f);
		assertEquals(0f, object.getShape().getAABB().getBot(), 0.1f);
	}
	@Test
	public void withstop() {
		World2D world = new Grid();
		
		World2DObject object = world.create(new World2DObjectData(new Rectangle(1, 1)));
		
		world.create(new World2DObjectData(new Rectangle(1, 0, 1, 1)));
		
		new MovementProcessor().move(world, object, 2.1f, 0, new MovementProcessor.MovementProcessorCallback() {
			@Override
			public boolean hit(float x, float y, World2DObject origin, World2DObject object) {
				System.out.println("triggered "+origin);
				
				return true;
			}
		});
		
		System.out.println(object);
		
		assertEquals(0f, object.getShape().getAABB().getLeft(), 0.1f);
		assertEquals(0f, object.getShape().getAABB().getBot(), 0.1f);
	}
}

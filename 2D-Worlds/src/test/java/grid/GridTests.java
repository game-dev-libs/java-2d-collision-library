package grid;

import org.junit.Test;

import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.grid.Grid;

import tests.WorldTest;

public class GridTests {
	@Test
	public void test() {
		new WorldTest() {
			
			@Override
			public World2D createEmptyWorld() {
				return new Grid();
			}
		}.runTests();
	}
}

package com.felixcool98.worlds.movement;

import com.badlogic.gdx.math.Vector2;
import com.felixcool98.worlds.Intersections;
import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.objects.World2DObject;

public class MovementProcessor {
	private float stepSize;
	
	
	public MovementProcessor() {
		this(0.1f);
	}
	public MovementProcessor(float stepSize) {
		this.stepSize = stepSize;
	}
	
	
	/**
	 * 
	 * @param world
	 * @param object
	 * @param x
	 * @param y
	 * @param callback
	 * @return 0 no collision<br>
	 * 1 callback was triggered but processing continued<br>
	 * 2 callback was triggered but processing stopped
	 */
	public int move(World2D world, World2DObject object, float x, float y, MovementProcessorCallback callback) {
		Vector2 vector = new Vector2(x, y);
		Vector2 restVector = new Vector2(vector);
		
		int steps = (int) (vector.len()/stepSize);
		float rest = vector.len() - steps*stepSize;
		
		restVector.setLength(rest);
		vector.setLength(stepSize);

		float xStepSize = vector.x;
		float yStepSize = vector.y;
		
		float xRest = restVector.x;
		float yRest = restVector.y;
		
		
		boolean callbackFired = false;
		
		Intersections intersections;
		
		for(int i = 0; i < steps; i++) {
			intersections = world.getIntersections(object);
			
			for(World2DObject other : intersections) {
				if(object.equals(other))
					continue;
				
				boolean ret = callback.hit(xStepSize, yStepSize, object, other);
				callbackFired = true;
				
				if(ret)
					return 2;
			}
			
			object.getShape().move(xStepSize, yStepSize);
		}
		
		intersections = world.getIntersections(object);
		
		for(World2DObject other : intersections) {
			if(object.equals(other))
				continue;
			
			boolean ret = callback.hit(xRest, yRest, object, other);
			callbackFired = true;
			
			if(ret)
				return 2;
		}
		
		object.getShape().move(xRest, yRest);
		
		if(callbackFired)
			return 1;
		
		return 0;
	}
	
	
	public static interface MovementProcessorCallback {
		/**
		 * called if the object hits any other object
		 * 
		 * @param x x that will be added if this callback returns true
		 * @param y y that will be added if this callback returns true
		 * @param origin the object that gets moved
		 * @param object the object the origin hit while moving
		 * @return if true stops the processing, doesn't move the object and no further callbacks will be called
		 */
		public boolean hit(float x, float y, World2DObject origin, World2DObject object);
	}
}

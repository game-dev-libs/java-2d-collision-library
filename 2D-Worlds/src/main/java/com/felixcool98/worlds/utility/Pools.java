package com.felixcool98.worlds.utility;

import java.util.HashSet;

import com.felixcool98.collections.pools.ConcurrentPool;
import com.felixcool98.collections.pools.Pool;
import com.felixcool98.worlds.objects.World2DObject;

public class Pools {
	public static Pool<HashSet<World2DObject>> HASH_SET_POOL = new ConcurrentPool<HashSet<World2DObject>>(10) {
		@Override
		public HashSet<World2DObject> create() {
			return new HashSet<World2DObject>();
		}
	};
}

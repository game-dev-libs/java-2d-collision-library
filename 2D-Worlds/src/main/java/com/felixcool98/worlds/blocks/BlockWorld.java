package com.felixcool98.worlds.blocks;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.felixcool98.aabb.Shape;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.worlds.Intersections;
import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.objects.World2DObject;
import com.felixcool98.worlds.objects.World2DObjectAdapter;
import com.felixcool98.worlds.objects.World2DObjectData;
import com.felixcool98.worlds.utility.Pools;

/**
 * world that only consists out of 1 wide and height blocks
 * 
 * @author felixcool98
 *
 * @param <T>
 */
public class BlockWorld implements World2D{
	private Map<String, BlockWorldObject> map = new HashMap<>();
	
	
	@Override
	public World2DObject create(World2DObjectData data) {
		if(!(data.getShape() instanceof Rectangle) || ((Rectangle) data.getShape()).getWidth() != 1 || ((Rectangle) data.getShape()).getHeight() != 1)
			throw new IllegalArgumentException("block world should only contains 1 width and height rectangles");
		
		BlockWorldObject object = new BlockWorldObject(data);
		
		map.put(toHashCode(object), object);
		
		return object;
	}


	@Override
	public void destroyAll() {
		for(BlockWorldObject object : map.values()) {
			object.destroy();
		}
		
		map.clear();
	}
	

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public Intersections getIntersections(Shape shape) {
		HashSet<World2DObject> intersections = Pools.HASH_SET_POOL.get();
		
		for(int x = (int) Math.floor(shape.getAABB().getLeft()); x < Math.ceil(shape.getAABB().getRight()); x++) {
			for(int y = (int) Math.floor(shape.getAABB().getBot()); y < Math.ceil(shape.getAABB().getTop()); y++) {
				World2DObject object = map.get(toHashCode(x, y));
				
				if(object == null)
					continue;
				
				intersections.add(object);
			}
		}
		
		
		if(shape.getAABB().getHeight() == 0) {
			for(int x = (int) Math.floor(shape.getAABB().getLeft()); x < Math.ceil(shape.getAABB().getRight()); x++) {
				World2DObject object = map.get(toHashCode(x, shape.getAABB().getBot()));
				
				if(object == null)
					continue;
				
				intersections.add(object);
			}
//			for(int x = (int) Math.floor(shape.getAABB().getLeft()); x < Math.ceil(shape.getAABB().getRight()); x++) {
//				World2DObject object = map.get(toHashCode(x, shape.getAABB().getBot()-1));
//				
//				if(object == null)
//					continue;
//				
//				intersections.add(object);
//			}
		}
		if(shape.getAABB().getWidth() == 0) {
			for(int y = (int) Math.floor(shape.getAABB().getBot()); y < Math.ceil(shape.getAABB().getTop()); y++) {
				World2DObject object = map.get(toHashCode(shape.getAABB().getLeft(), y));
				
				if(object == null)
					continue;
				
				intersections.add(object);
			}
//			for(int y = (int) Math.floor(shape.getAABB().getBot()); y < Math.ceil(shape.getAABB().getTop()); y++) {
//				World2DObject object = map.get(toHashCode(shape.getAABB().getLeft()-1, y));
//				
//				if(object == null)
//					continue;
//				
//				intersections.add(object);
//			}
		}
		
		return Intersections.create(intersections);
	}
	
	
	@Override
	public String toString() {
		return "BlockWorld - size: "+size();
	}
	
	
	public class BlockWorldObject extends World2DObjectAdapter {
		private Shape old;
		
		public BlockWorldObject(World2DObjectData data) {
			super(data);
			
			old = data.getShape().clone();
		}
		
		
		@Override
		public void destroy() {
			if(isDestroyed())
				return;
			
			super.destroy();
			
			map.remove(toHashCode(this));
		}
		

		@Override
		public void bboxChanged() {
			if(!(getShape() instanceof Rectangle) || ((Rectangle) getShape()).getWidth() != 1 || ((Rectangle) getShape()).getHeight() != 1)
				throw new IllegalArgumentException("block world should only contains 1 width and height rectangles");
			
			if(old != null)
				map.remove(toHashCode(old));
			map.put(toHashCode(this), this);
			
			old = getShape().clone();
		}
	}
	
	
	//======================================================================
	// static helper methods
	//======================================================================
	
	private static String toHashCode(World2DObject object) {
		return toHashCode(object.getShape());
	}
	private static String toHashCode(Shape shape) {
		return toHashCode(shape.getAABB().getLeft(), shape.getAABB().getRight(),
				shape.getAABB().getBot(), shape.getAABB().getTop());
	}
	private static String toHashCode(float left, float right, float bot, float top) {
		return toHashCode(left, bot);
	}
	private static String toHashCode(float x, float y) {
		return (int)x+" "+(int)y;
	}
}

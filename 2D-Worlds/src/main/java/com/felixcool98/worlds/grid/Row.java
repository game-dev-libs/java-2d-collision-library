package com.felixcool98.worlds.grid;

public class Row {
	private Grid grid;
	
	private Region parent;
	private int x;
	
	private Cell[] cells;
	
	private int initializedCells;
	
	
	public Row(Grid grid, Region parent, int x) {
		this.grid = grid;
		this.parent = parent;
		this.x = x;
	}
	
	
	//======================================================================
	// creating cells
	//======================================================================
	
	public void createCellIfNeeded(int y) {
		if(getState(y) == 0)
			return;
		if(getState(y) == 2)
			cells = new Cell[grid.getRegionSize()];
		
		cells[y] = new Cell(this, y);
		initializedCells++;
	}
	
	
	//======================================================================
	// destroying cells
	//======================================================================
	
	public void destroyCell(int y) {
		if(getState(y) > 0)
			return;
		
		cells[y] = null;
		initializedCells--;
		
		if(initializedCells == 0) 
			parent.destroyRow(x);
	}
	
	
	//======================================================================
	// getting cells
	//======================================================================
	
	public Cell getCell(int y){
		if(getState(y) > 0)
			return null;
		
		return cells[y];
	}
	
	
	/**
	 * 
	 * @param y
	 * @return <br>
	 * 0 cell exists<br>
	 * 1 cell doesn't exist<br>
	 * 2 cell array is null<br>
	 */
	private int getState(int y) {
		if(cells == null)
			return 2;
		if(cells[y] == null)
			return 1;
		
		return 0;
	}
}

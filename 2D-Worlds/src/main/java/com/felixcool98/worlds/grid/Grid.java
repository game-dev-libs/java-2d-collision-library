package com.felixcool98.worlds.grid;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.felixcool98.aabb.AABB;
import com.felixcool98.aabb.Shape;
import com.felixcool98.collections.OneTimeList;
import com.felixcool98.worlds.Intersections;
import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.objects.World2DObject;
import com.felixcool98.worlds.objects.World2DObjectAdapter;
import com.felixcool98.worlds.objects.World2DObjectData;
import com.felixcool98.worlds.utility.Pools;

/**
 * A {@link World2D} that is made up out of a grid<br>
 * movement of objects will be automatically handled as long as the collision mask object won't change
 * 
 * @author felixcool98
 *
 * @param <T>
 */
public class Grid implements World2D {
	private int regionSize;
	private float cellSize;
	
	private Map<String, Region> regions = new HashMap<>();
	
	private List<GridWorldObject> objects = new LinkedList<>();
	
	
	public Grid() {
		this(512, 16);
	}
	public Grid(int regionSize, float cellSize) {
		this.regionSize = regionSize;
		this.cellSize = cellSize;
	}
	
	
	//======================================================================
	// adding objects
	//======================================================================
	
	@Override
	public World2DObject create(World2DObjectData data) {
		GridWorldObject object = new GridWorldObject(data);
		
		internalAdd(object);
		objects.add(object);
		
		return object;
	}
	
	private void internalAdd(World2DObject object) {
		AABB aabb = object.getShape().getAABB();
		
		for(float x = aabb.getLeft(); x < aabb.getRight(); x++) {
			for(float y = aabb.getBot(); y < aabb.getTop(); y++) {
				createCellIfNeeded(x, y);
				getCellAt(x, y).add(object);
			}
		}
	}
	
	
	//======================================================================
	// removing objects
	//======================================================================
	
	void remove(GridWorldObject object) {
		objects.remove(object);
		internalRemove(object, true);
	}
	private OneTimeList<Cell> internalRemove(GridWorldObject object, boolean destroyIfEmpty) {
		OneTimeList<Cell> changed = new OneTimeList<>();
		
		AABB aabb = object.getShape().getAABB();
		
		for(float x = aabb.getLeft(); x < aabb.getRight(); x++) {
			for(float y = aabb.getLeft(); y < aabb.getRight(); y++) {
				createCellIfNeeded(x, y);
				
				Cell cell = getCellAt(x, y);
				
				cell.remove(object, destroyIfEmpty);
				
				changed.add(cell);
			}
		}
		
		return changed;
	}
	
	@Override
	public void destroyAll() {
		regions.clear();
		
		for(GridWorldObject object : objects) {
			object.destroy();
		}
		
		objects.clear();
	}

	
	//======================================================================
	// getter
	//======================================================================
	
	@Override
	public int size() {
		return objects.size();
	}

	@Override
	public Intersections getIntersections(Shape shape) {
		HashSet<World2DObject> intersections = Pools.HASH_SET_POOL.get();
		
		AABB aabb = shape.getAABB();
		
		for(float x = aabb.getLeft(); x < aabb.getRight(); x+=cellSize) {
			for(float y = aabb.getBot(); y < aabb.getTop(); y+=cellSize) {
				Cell cell = getCellAt(x, y);
				
				if(cell == null)
					continue;
				
				cell.getIntersections(shape, intersections);
				
			}
		}
		if(shape.getAABB().getHeight() == 0) {
			for(int x = (int) Math.floor(shape.getAABB().getLeft()); x < Math.ceil(shape.getAABB().getRight()); x++) {
				Cell cell = getCellAt(x, shape.getAABB().getBot());
				
				if(cell == null)
					continue;
				
				cell.getIntersections(shape, intersections);
			}
		}
		if(shape.getAABB().getWidth() == 0) {
			for(int y = (int) Math.floor(shape.getAABB().getBot()); y < Math.ceil(shape.getAABB().getTop()); y++) {
				Cell cell = getCellAt(shape.getAABB().getLeft(), y);
				
				if(cell == null)
					continue;
				
				cell.getIntersections(shape, intersections);
			}
		}
		
		return Intersections.create(intersections);
	}
	
	//mine
	
	public int getRegionSize() {
		return regionSize;
	}
	
	
	//======================================================================
	// internal
	//======================================================================
	
	private void createCellIfNeeded(float x, float y) {
		int regionX = (int) (x / regionSize);
		int regionY = (int) (y / regionSize);
		
		if(x < 0)
			regionX--;
		if(y < 0)
			regionY--;
		
		int cellX = Math.abs((int) (x % regionSize / cellSize));
		int cellY = Math.abs((int) (y % regionSize / cellSize));
		
		createIfNeeded(regionX, regionY);
		
		regions.get(toString(regionX, regionY)).createCellIfNeeded(cellX, cellY);
	}
	private Cell getCellAt(float x, float y) {
		int regionX = (int) (x / regionSize);
		int regionY = (int) (y / regionSize);
		
		if(x < 0)
			regionX--;
		if(y < 0)
			regionY--;
		
		int cellX = Math.abs((int) (x % regionSize / cellSize));
		int cellY = Math.abs((int) (y % regionSize / cellSize));
		
		Region region = regions.get(toString(regionX, regionY));
		
		if(region == null)
			return null;
		
		return region.getCell(cellX, cellY);
	}
	
	private String toString(int x, int y) {
		return x+" "+y;
	}
	
	
	//======================================================================
	// regions
	//======================================================================
	
	public void destroyRegion(int x, int y) {
		regions.remove(toString(x, y));
	}
	private void createIfNeeded(int x, int y) {
		if(regions.containsKey(toString(x, y)))
			return;
		
		regions.put(toString(x, y), new Region(this, x, y));
	}
	
	
	//======================================================================
	// object overides
	//====================================================================== 
	
	@Override
	public String toString() {
		return "Grid - "+regions.size()+" regions | "+size()+" objects";
	}
	
	
	public class GridWorldObject extends World2DObjectAdapter {
		public GridWorldObject(World2DObjectData data) {
			super(data);
		}

		
		@Override
		public void bboxChanged() {
			internalRemove(this, false);
			internalAdd(this);
		}
		
		
		@Override
		public void destroy() {
			if(isDestroyed())
				return;
			
			super.destroy();
			
			remove(this);
		}
	}
}

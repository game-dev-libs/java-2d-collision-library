package com.felixcool98.worlds.grid;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import com.felixcool98.aabb.Shape;
import com.felixcool98.worlds.objects.World2DObject;

public class Cell {
	private Row parent;
	
	private int y;
	
	private List<World2DObject> objects = new LinkedList<>();
	
	
	public Cell(Row parent, int y) {
		this.parent = parent;
		this.y = y;
	}
	
	
	public void add(World2DObject object) {
		objects.add(object);
	}
	
	public void remove(World2DObject object, boolean destroyIfEmpty) {
		objects.remove(object);
		
		if(destroyIfEmpty)
			destroyIfEmpty();
	}
	
	
	public void destroyIfEmpty() {
		if(objects.isEmpty())
			parent.destroyCell(y);
	}
	
	
	public void getIntersections(Shape shape, HashSet<World2DObject> list){
		for(World2DObject object : objects) {
			if(list.contains(object))
				continue;
			
			if(!object.getShape().overlaps(shape))
				continue;			
			
			list.add(object);
		}
	}
}

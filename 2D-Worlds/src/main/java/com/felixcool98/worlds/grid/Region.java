package com.felixcool98.worlds.grid;

public class Region {
	private Grid grid;
	
	private int x, y;
	
	private Row[] rows;
	
	private int initializedRows;
	
	
	public Region(Grid grid, int x, int y) {
		this.grid = grid;
		
		this.x = x;
		this.y = y;
	}
	
	
	//======================================================================
	// create rows
	//======================================================================
	
	public void createRowIfNeeded(int x) {
		if(getState(x) == 0)
			return;
		if(getState(x) == 2)
			rows = new Row[grid.getRegionSize()];
		
		rows[x] = new Row(grid, this, x);
		initializedRows++;
	}
	
	
	//======================================================================
	// destroy rows
	//======================================================================
	
	public void destroyRow(int x) {
		if(getState(x) > 0)
			return;
		
		rows[x] = null;
		initializedRows --;
		
		if(initializedRows == 0)
			grid.destroyRegion(this.x, this.y);
	}
	
	
	//======================================================================
	// cells
	//======================================================================
	
	public Cell getCell(int x, int y) {
		if(getState(x) > 0)
			return null;
		
		return rows[x].getCell(y);
	}
	
	
	public void createCellIfNeeded(int x, int y) {
		createRowIfNeeded(x);
		
		rows[x].createCellIfNeeded(y);
	}
	
	
	/**
	 * 
	 * @param x
	 * @return <br>
	 * 0 row exists<br>
	 * 1 row doesn't exist<br>
	 * 2 row array is null<br>
	 */
	private int getState(int x) {
		if(rows == null)
			return 2;
		if(rows[x] == null)
			return 1;
		
		return 0;
	}
}

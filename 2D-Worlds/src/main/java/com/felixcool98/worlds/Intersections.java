package com.felixcool98.worlds;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import com.felixcool98.collections.pools.ConcurrentPool;
import com.felixcool98.collections.pools.Pool;
import com.felixcool98.collections.pools.Poolable;
import com.felixcool98.worlds.objects.World2DObject;
import com.felixcool98.worlds.utility.Pools;

public class Intersections implements Iterable<World2DObject>, Poolable {
	public static Pool<Intersections> POOL = new ConcurrentPool<Intersections>(10) {
		@Override
		public Intersections create() {
			return new Intersections();
		}
	};
	
	
	public static Intersections create() {
		return POOL.get();
	}
	public static Intersections create(HashSet<World2DObject> intersections) {
		return new Intersections(intersections);
	}
	public static Intersections create(ArrayList<World2DObject> intersections) {
		return new Intersections(intersections);
	}
	
	
	private ArrayList<World2DObject> intersections;
	
	
	private Intersections(HashSet<World2DObject> intersections) {
		this(new ArrayList<World2DObject>(intersections));
		
		intersections.clear();
		Pools.HASH_SET_POOL.free(intersections);
	}
	private Intersections() {
		this(new ArrayList<World2DObject>());
	}
	private Intersections(ArrayList<World2DObject> intersections) {
		this.intersections = intersections;
	}
	
	
	public void add(World2DObject object) {
		intersections.add(object);
	}
	public void remove(World2DObject object) {
		intersections.remove(object);
	}
	
	
	public ArrayList<World2DObject> getIntersections() {
		return intersections;
	}
	
	
	public boolean isEmpty() {
		return intersections.isEmpty();
	}
	public int size() {
		return intersections.size();
	}
	
	
	public Intersections getDrawables() {
		Intersections drawables = create();
		
		for(World2DObject obj : getIntersections()) {
			if(obj.isDrawable())
				drawables.add(obj);
		}
		
		return drawables;
	}
	
	
	@Override
	public Iterator<World2DObject> iterator() {
		return intersections.iterator();
	}
	@Override
	public void reset() {
		intersections.clear();
	}
	
	
	/**
	 * should be called when no longer needed<br>
	 * don't use this object anymore after calling this method
	 */
	public void free() {
		POOL.free(this);
	}
}

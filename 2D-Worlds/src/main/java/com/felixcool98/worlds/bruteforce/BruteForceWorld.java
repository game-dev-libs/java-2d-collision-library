package com.felixcool98.worlds.bruteforce;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.aabb.Shape;
import com.felixcool98.worlds.Intersections;
import com.felixcool98.worlds.World2D;
import com.felixcool98.worlds.objects.World2DObject;
import com.felixcool98.worlds.objects.World2DObjectAdapter;
import com.felixcool98.worlds.objects.World2DObjectData;

/**
 * slow world implementation<br>
 * <br>
 * only contains a list with all objects<br>
 * <br>
 * calling move is unneeded
 * 
 * @author felixcool98
 *
 * @param <T>
 */
public class BruteForceWorld implements World2D {
	private List<BruteForceWorldObject> objects = new LinkedList<>();
	

	public BruteForceWorld() {
		
	}
	
	
	//======================================================================
	// adding objects
	//======================================================================
	
	@Override
	public BruteForceWorldObject create(World2DObjectData data) {
		BruteForceWorldObject object = new BruteForceWorldObject(data);
		
		objects.add(object);
		
		return object;
	}
	
	
	//======================================================================
	// removing objects
	//======================================================================

	@Override
	public void destroyAll() {
		for(BruteForceWorldObject object : objects) {
			object.destroy();
		}
	}
	
	
	//======================================================================
	// getter
	//======================================================================

	//general data
	
	@Override
	public int size() {
		return objects.size();
	}

	//intersections
	
	@Override
	public Intersections getIntersections(Shape shape) {
		Intersections intersections = Intersections.create();
		
		for(World2DObject object : objects) {
			if(object.getShape().overlaps(shape))
				intersections.add(object);
		}
		
		return intersections;
	}
	
	
	//======================================================================
	// checks
	//======================================================================
	
	@Override
	public boolean isEmpty() {
		return objects.isEmpty();
	}
	
	
	//======================================================================
	// object overrides
	//======================================================================
	
	@Override
	public String toString() {
		return "BruteForceWorld - size: "+size();
	}
	
	
	public class BruteForceWorldObject extends World2DObjectAdapter {

		public BruteForceWorldObject(World2DObjectData data) {
			super(data);
		}

		
		@Override
		public void bboxChanged() {}
		
		
		@Override
		public void destroy() {
			if(isDestroyed())
				return;
			
			super.destroy();
			
			objects.remove(this);
		}
	}
}

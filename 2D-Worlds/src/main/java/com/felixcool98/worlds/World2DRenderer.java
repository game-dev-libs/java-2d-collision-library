package com.felixcool98.worlds;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.worlds.objects.World2DObject;
import com.felixcool98.worlds.objects.World2DObject.World2DObjectChangeListener;

public class World2DRenderer implements World2DObjectChangeListener {
	private OrthographicCamera camera;
	private OrthographicCamera debugCamera;
	
	private Batch batch = new SpriteBatch();
	private ShapeRenderer renderer = new ShapeRenderer();
	
	private World2D world;
	
	
	public World2DRenderer(World2D world, float width, float height) {
		this(world, new OrthographicCamera(width, height), null);
	}
	public World2DRenderer(World2D world, OrthographicCamera camera) {
		this(world, camera, null);
	}
	public World2DRenderer(World2D world, OrthographicCamera camera, OrthographicCamera debugCamera) {
		renderer.setAutoShapeType(true);
		
		setCamera(camera);
		setDebugCamera(debugCamera);
		setWorld(world);
	}
	
	
	public void setCamera(OrthographicCamera camera) {
		this.camera = camera;
	}
	public void setDebugCamera(OrthographicCamera camera) {
		this.debugCamera = camera;
	}
	public void setWorld(World2D world) {
		this.world = world;
	}
	
	
	protected Intersections getDrawables(){
		if(camera == null)
			return null;
		
		camera.update();
		
		Intersections objects = world.getIntersections(getBoundingBox());
		
		return objects.getDrawables();
	}
	protected Intersections getDebugDrawables(){
		if(camera == null)
			return null;
		
		camera.update();
		
		Intersections objects = world.getIntersections(getBoundingBox());
		
		return objects;
	}
	
	public void render() {
		Intersections drawables = getDrawables();
		
		if(drawables == null || drawables.isEmpty())
			return;
		
		if(camera == null)
			return;
		
		camera.update();
		
		batch.setProjectionMatrix(camera.combined);
		
		batch.begin();{
			for(World2DObject drawable : drawables) {
				drawable.draw(batch);
			}
		}batch.end();
	}
	public void debugRender() {
		Intersections drawables = getDebugDrawables();
		
		if(drawables == null || drawables.isEmpty())
			return;
		
		OrthographicCamera camera = debugCamera;
		
		if(camera == null)
			camera = this.camera;
		
		if(camera == null)
			return;
		
		camera.update();
		
		renderer.setProjectionMatrix(camera.combined);
		
		renderer.begin();{
			for(World2DObject drawable : drawables) {
				drawable.debugDraw(renderer);
			}
		}renderer.end();
	}
	
	
	public Rectangle getBoundingBox() {
		return new Rectangle(camera.position.x-camera.viewportWidth/2f, camera.position.y-camera.viewportHeight/2f, camera.viewportWidth, camera.viewportHeight);
	}
	
	
	public void setPositionCentered(float x, float y) {
		camera.position.set(x, y, 0);
	}
	
	
	public OrthographicCamera getCamera() {
		return camera;
	}
	
	
	@Override
	public void changed(World2DObject object) {
		setPositionCentered(object.getShape().getAABB().getCenterX(), object.getShape().getAABB().getCenterY());
	}
}

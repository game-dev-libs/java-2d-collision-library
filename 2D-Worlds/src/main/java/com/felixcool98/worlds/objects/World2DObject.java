package com.felixcool98.worlds.objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.felixcool98.aabb.Shape;
import com.felixcool98.worlds.World2D;

/**
 * <h1>change detection</h1><br>
 * if change detection is enabled {@link World2DObject#bboxChanged()} will be called if the bounding box of this object changes<br>
 * this allows the {@link World2D} this object belongs to to process movement of this object
 * 
 * @author felixcool98
 */
public interface World2DObject {
	//======================================================================
	// getter
	//======================================================================
	
	public Shape getShape();
	
	public Object getUserObject();
	
	
	//======================================================================
	// setter
	//======================================================================
	
	public void setShape(Shape shape);
	
	public void setUserObject(Object object);
	
	
	//======================================================================
	// checks
	//======================================================================
	
	public default boolean overlaps(World2DObject object) {
		return getShape().overlaps(object.getShape());
	}
	
	
	//======================================================================
	// destroying objects
	//======================================================================
	
	public void destroy();
	
	public boolean isDestroyed();
	
	
	//======================================================================
	// changed
	//======================================================================
	
	/**
	 * calling this method will activate change detection on the object<br>
	 * default state is enabled<br>
	 * reenabling detection will call {@link World2DObject#bboxChanged()} once
	 */
	public void enableChangeDetection();
	/**
	 * calling this method will deactivate change detection on the object<br>
	 * default state is enabled<br>
	 * <br>
	 * disabling change detection can result in unpredictable behavior
	 */
	@Deprecated
	public void disableChangeDetection();
	
	/**
	 * @return true if the object should detect changes to its bounding box else false
	 */
	public boolean isDetectingChanges();
	
	/**
	 * called whenever the bounding box of this object changes as long as change detection is activated
	 */
	public void bboxChanged();
	
	
	//======================================================================
	// drawing
	//======================================================================
	
	public void setDrawable(Drawable drawable);
	public void setDepth(int depth);
	
	public int getDepth();
	
	public boolean isDrawable();
	
	/**
	 * draws the object in the world with its given size
	 * 
	 * @param batch
	 */
	public void draw(Batch batch);
	/**
	 * draws the outline of the object in the world
	 * 
	 * @param renderer
	 */
	public void debugDraw(ShapeRenderer renderer);
	
	
	//======================================================================
	// listeners
	//======================================================================
	
	public void addListener(World2DObjectChangeListener listener);
	
	public void removeListener(World2DObjectChangeListener listener);
	
	public static interface World2DObjectChangeListener {
		public void changed(World2DObject object);
	}
	
	public void callListeners();
}

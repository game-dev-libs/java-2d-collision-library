package com.felixcool98.worlds.objects;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.felixcool98.aabb.AABB;
import com.felixcool98.aabb.Shape;
import com.felixcool98.aabb.Shape.ShapeChangeListener;

public abstract class World2DObjectAdapter implements World2DObject {
	private boolean destroyed = false;
	
	private boolean detectChanges = true;
	
	private Object userObject;
	
	private Drawable drawable;
	private int depth;
	
	private List<World2DObjectChangeListener> listeners = new LinkedList<>();
	
	private Shape shape;
	
	private ShapeChangeListener shapeListener;
	
	
	
	public World2DObjectAdapter(World2DObjectData data) {
		this(data.getShape(), data.getDrawable());
	}
	public World2DObjectAdapter(Shape shape, Drawable drawable) {
		if(shape == null)
			throw new IllegalArgumentException("Shape can't be null");
		
		shapeListener = new ShapeChangeListener() {
			@Override
			public void shapeChanged(Shape shape) {
				if(isDetectingChanges()) {
					bboxChanged();
					
					callListeners();
				}
			}
		};
		
		setShape(shape);
		
		this.drawable = drawable;
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	
	@Override
	public Shape getShape() {
		return shape;
	}
	
	@Override
	public Object getUserObject() {
		return userObject;
	}

	
	//======================================================================
	// setter
	//======================================================================
	
	@Override
	public void setShape(Shape shape) {
		if(shape == null)
			throw new IllegalArgumentException("Shape can't be null");
		
		Shape old = getShape();
		
		if(old != null) 
			old.removeListener(shapeListener);
		
		this.shape = shape.clone();
		
		this.shape.addListener(shapeListener);
		
		if(old != null && old.getAABB().equals(shape.getAABB()))
			return;
		
		bboxChanged();
		callListeners();
	}
	
	@Override
	public void setUserObject(Object object) {
		this.userObject = object;
	}
	
	
	//======================================================================
	// destroying objects
	//======================================================================
	
	@Override
	public boolean isDestroyed() {
		return destroyed;
	}
	@Override
	public void destroy() {
		this.destroyed = true;
	}
	
	
	//======================================================================
	// changed
	//======================================================================
	
	@Override
	public void disableChangeDetection() {
		detectChanges = false;
	}
	@Override
	public void enableChangeDetection() {
		if(detectChanges)
			return;
		
		detectChanges = true;
		
		bboxChanged();
		callListeners();
	}
	
	@Override
	public boolean isDetectingChanges() {
		return detectChanges;
	}
	
	@Override
	public void callListeners() {
		for(World2DObjectChangeListener listener : listeners) {
			listener.changed(World2DObjectAdapter.this);
		}
	}
	
	
	//======================================================================
	// drawing
	//======================================================================
	
	@Override
	public void debugDraw(ShapeRenderer renderer) {
		AABB aabb = getShape().getAABB();
		
		renderer.rect(aabb.getLeft(), aabb.getBot(), aabb.getRight()-aabb.getLeft(), aabb.getTop()-aabb.getBot());
	}
	
	@Override
	public void draw(Batch batch) {
		if(!isDrawable())
			return;
		
		AABB aabb = getShape().getAABB();
		
		drawable.draw(batch, aabb.getLeft(), aabb.getBot(), aabb.getRight()-aabb.getLeft(), aabb.getTop()-aabb.getBot());
	}
	
	@Override
	public int getDepth() {
		return depth;
	}
	
	@Override
	public boolean isDrawable() {
		return drawable != null;
	}
	
	@Override
	public void setDrawable(Drawable drawable) {
		this.drawable = drawable;
	}
	@Override
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
	
	
	//======================================================================
	// listeners
	//======================================================================
	
	@Override
	public void addListener(World2DObjectChangeListener listener) {
		listeners.add(listener);
	}
	@Override
	public void removeListener(World2DObjectChangeListener listener) {
		listeners.remove(listener);
	}
	
	@Override
	public void bboxChanged() {}

	
	//======================================================================
	// object override
	//======================================================================
	
	@Override
	public String toString() {
		return "World2DObjectAdapter ("+getShape().toString()+")";
	}
}

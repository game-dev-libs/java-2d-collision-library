package com.felixcool98.worlds.objects;

import com.felixcool98.utility.filter.Filter;

public class World2DDrawableFilter implements Filter<World2DObject> {
	@Override
	public boolean valid(World2DObject object) {
		return object.isDrawable();
	}
}

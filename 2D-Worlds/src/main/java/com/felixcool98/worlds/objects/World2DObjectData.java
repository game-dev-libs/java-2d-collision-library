package com.felixcool98.worlds.objects;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.felixcool98.aabb.Shape;

public class World2DObjectData {
	private Shape shape;
	
	private Drawable drawable;
	
	
	public World2DObjectData(Shape shape) {
		this(shape, null);
	}
	public World2DObjectData(Shape shape, Drawable drawable) {
		this.shape = shape;
		
		this.drawable = drawable;
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	
	public Shape getShape() {
		return shape;
	}
	
	public Drawable getDrawable() {
		return drawable;
	}
	
	
	//======================================================================
	// setter
	//======================================================================
	
	public void setShape(Shape shape) {
		this.shape = shape;
	}
	
	public void setDrawable(Drawable drawable) {
		this.drawable = drawable;
	}
}

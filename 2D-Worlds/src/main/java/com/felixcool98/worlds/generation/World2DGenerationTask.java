package com.felixcool98.worlds.generation;

import com.felixcool98.worlds.World2D;

public interface World2DGenerationTask {
	public void generate(World2D world, float x, float y);
}

package com.felixcool98.worlds.generation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.felixcool98.worlds.World2D;

public class World2DGenerator {
	private Map<String, World2DGenerationTask> tasks = new HashMap<>();
	
	private List<GenerationTask> taskList = new LinkedList<>();
	
	
	public void registerTask(String name, World2DGenerationTask task) {
		tasks.put(name, task);
	}
	
	
	public void addTask(World2D world, String task, float x, float y) {
		taskList.add(new GenerationTask() {
			@Override
			public void generate(World2D world) {
				tasks.get(task).generate(world, x, y);
			}
		});
	}
	public void addTask(World2D world, String task, float x, float y, float width, float height) {
		taskList.add(new GenerationTask() {
			@Override
			public void generate(World2D world) {
				((World2DSizeableGenerationTask) tasks.get(task)).generate(world, x, y, width, height);
			}
		});
	}
	public void generate(World2D world) {
		for(GenerationTask task : taskList) {
			task.generate(world);
		}
	}
	
	
	
	public static interface GenerationTask {
		public void generate(World2D world);
	}
}

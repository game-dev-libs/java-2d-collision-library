package com.felixcool98.worlds;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.felixcool98.gdxutility.ScissorUtils;
import com.felixcool98.worlds.objects.World2DObject;
import com.felixcool98.worlds.objects.World2DObject.World2DObjectChangeListener;

/**
 * An actor that is able to display a {@link World2D}<br>
 * implements {@link BBoxChanged} to allow the view to follow a {@link World2DObject}
 * 
 * @author felixcool98
 */
public class World2DActor extends Actor implements World2DObjectChangeListener {
	private World2DRendererDrawable drawable = new World2DRendererDrawable();
	
	
	/**
	 * @param world the world this actor should display
	 */
	public void setWorld(World2D world) {
		drawable.setWorld(world);
	}
	/**
	 * @param x the x coordinate of the view (in world coordinates)
	 * @param y the y coordinate of the view (in world coordinates)
	 */
	public void setViewPosition(float x, float y) {
		drawable.setPosition(x, y);
	}
	/**
	 * @param width the width of the view (in world coordinates)
	 * @param height the height of the view (in world coordinates)
	 */
	public void setViewSize(float width, float height) {
		drawable.setSize(width, height);
	}
	
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		ScissorUtils.prepare(batch, this);
		
		drawable.drawWithoutScissors(batch, getX(), getY(), getWidth(), getHeight());
		
		ScissorUtils.after(getStage(), batch);
	}
	
	@Override
	public void changed(World2DObject object) {
		drawable.setCenter(object.getShape().getAABB().getCenterX(), object.getShape().getAABB().getCenterY());
	}
}

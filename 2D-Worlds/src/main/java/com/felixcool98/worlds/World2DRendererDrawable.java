package com.felixcool98.worlds;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.gdxutility.ScissorUtils;
import com.felixcool98.worlds.objects.World2DObject;

/**
 * a {@link Drawable} that is able to display a {@link World2D}
 * 
 * @author felixcool98
 */
public class World2DRendererDrawable extends BaseDrawable {
	private com.felixcool98.aabb.shapes.Rectangle bounds = new com.felixcool98.aabb.shapes.Rectangle(0, 0, 1, 1);
	private OrthographicCamera temp = new OrthographicCamera();
	
	
	private World2D world;
	
	
	public void setPosition(float x, float y) {
		bounds.setPosition(x, y);
	}
	public void addPosition(float x, float y) {
		bounds.move(x, y);
	}
	public void setCenter(float x, float y) {
		bounds.setCenter(x, y);
	}
	public void setSize(float width, float height) {
		bounds.setSize(width, height);
	}
	public void setWorld(World2D world) {
		this.world = world;
	}
	
	
	protected Intersections getDrawables(){
		Intersections objects = world.getIntersections(getBoundingBox());
		
		return objects.getDrawables();
	}
	
	
	@Override
	public void draw(Batch batch, float x, float y, float width, float height) {
		Intersections drawables = getDrawables();
		
		if(drawables == null || drawables.isEmpty())
			return;
		
		Rectangle local = updateCamera(x, y, width, height);

		batch.setProjectionMatrix(temp.combined);
		
		ScissorUtils.prepare(temp, batch, local);
		
		draw(drawables, batch);
		
		ScissorUtils.after(batch);
	}
	public void drawWithoutScissors(Batch batch, float x, float y, float width, float height) {
		Intersections drawables = getDrawables();
		
		if(drawables == null || drawables.isEmpty())
			return;
		
		updateCamera(x, y, width, height);
		
		batch.setProjectionMatrix(temp.combined);
		
		draw(drawables, batch);
	}
	private Rectangle updateCamera(float x, float y, float width, float height) {
		float xFactor = GdxUtils.getWidth()*getWidth()/width;
		float yFactor = GdxUtils.getHeight()*getHeight()/height;
		
		float xOffset = getX();
		float yOffset = getY();
		
		temp.position.x = xFactor/2f-xFactor*(x/GdxUtils.getWidth())+xOffset;
		temp.position.y = yFactor/2f-yFactor*(y/GdxUtils.getHeight())+yOffset;
		
		temp.viewportWidth = xFactor;
		temp.viewportHeight = yFactor;
		
		temp.update();
		
		return new Rectangle(temp.position.x+xFactor/2f*getX(), temp.position.y+yFactor/2f*getY(), xFactor*getWidth()/2f, yFactor*getHeight()/2f);
	}
	private void draw(Intersections drawables, Batch batch) {
		for(World2DObject drawable : drawables) {
			drawable.draw(batch);
		}
	}
	
	
	public Vector2 mouseToWorldCoordinates(Vector2 mouse, Vector2 drawPosition, Vector2 drawSize) {
		mouse = new Vector2(mouse);
		
		mouse.sub(drawPosition);
		
		float xFactor = drawSize.x/bounds.getWidth();
		float yFactor = drawSize.y/bounds.getHeight();
		
		mouse.x /= xFactor;
		mouse.y /= yFactor;
		
		mouse.x += bounds.getX();
		mouse.y += bounds.getY();
		
		return mouse;
	}
	
	
	public void debugDrawOutline(ShapeRenderer renderer) {
		renderer.set(ShapeType.Line);
		renderer.rect(getX(), getY(), getWidth(), getHeight());
	}
	
	
	public com.felixcool98.aabb.shapes.Rectangle getBoundingBox() {
		return bounds;
	}
	
	private float getX() {
		return bounds.getX();
	}
	private float getY() {
		return bounds.getY();
	}
	
	private float getWidth() {
		return bounds.getWidth();
	}
	private float getHeight() {
		return bounds.getHeight();
	}
}

package com.felixcool98.worlds;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.felixcool98.aabb.Shape;
import com.felixcool98.aabb.shapes.Rectangle;
import com.felixcool98.worlds.objects.World2DObject;
import com.felixcool98.worlds.objects.World2DObjectAdapter;
import com.felixcool98.worlds.objects.World2DObjectData;

/**
 * interface for 2D Worlds<br>
 * <br>
 * 2D Worlds are meant for easy collision detection and rendering<br>
 * <br>
 * Every 2dWorld consists of {@link World2DObject}s. The implementation of these objects 
 * differs from world type to world type but generally {@link World2DObjectAdapter} is extended. 
 * Any special cases should be mentioned in the documentation of the world type.<br>
 * <br>
 * <h1>{@link World2DObject}s</h1>
 * <br>
 * These objects represent any object inside of a 2d world. They only encapsule the axis aligned 
 * bounding box of an object.<br>
 * There are methods for moving and changing the size of the bounding box.<br>
 * <br>
 * For fast collision detection the {@link World2DObject#overlaps(float, float, float, float)} methods can be used.<br>
 * <br>
 * Calling {@link World2DObject#destroy()} will remove the object from the world.<br>
 * <br>
 * You can also change the depth of the object. Depth is used to change the draw order of the objects.<br>
 * Objects with higher depths get draw in the background.<br>
 * <br>
 * If a drawable is set {@link World2DObject#draw(com.badlogic.gdx.graphics.g2d.Batch)} can be used to draw the object directly at its world position. 
 * {@link Drawable#draw(com.badlogic.gdx.graphics.g2d.Batch, float, float, float, float)} will be called with the size and world position of the object.<br>
 * {@link World2DObject#debugDraw(com.badlogic.gdx.graphics.glutils.ShapeRenderer)} can be used to only draw the outline of the object.<br>
 * <br>
 * Listeners can be used to notify f.E. cameras that the objects position has changed.<br>
 * 
 * @author felixcool98
 *
 * @param <T>
 */
public interface World2D {
	//======================================================================
	// adding objects
	//======================================================================
	
	public default World2DObject create(Shape shape) {
		return create(new World2DObjectData(shape));
	}
	public World2DObject create(World2DObjectData data);
	
	
	//======================================================================
	// removing objects
	//======================================================================
	
	/**
	 * Destroys all objects in the world
	 */
	public void destroyAll();
	
	
	//======================================================================
	// getter
	//======================================================================
	
	//general data
	
	/**
	 * @return the total number of {@link World2DObject}s in this world
	 */
	public int size();
	
	//intersections
	
	public default Intersections getIntersections(float x, float y){
		Intersections objects = Intersections.create();
		
		for(World2DObject object : getIntersections(new Rectangle(x-0.1f, y-0.1f, 0.2f, 0.2f))) {
			if(!object.getShape().contains(x, y))
				continue;
			
			objects.add(object);
		}
		
		return objects;
	}
	
	public default Intersections getIntersections(World2DObject object){
		Intersections intersections = getIntersections(object.getShape());
		
		intersections.remove(object);
		
		return intersections;
	}
	public Intersections getIntersections(Shape shape);
	
	
	//======================================================================
	// checks
	//======================================================================
	
	/**
	 * @return true if the world has no objects, false if it has any
	 */
	public default boolean isEmpty() {
		return size() == 0;
	}
}

package com.felixcool98.aabb;

import java.util.LinkedList;
import java.util.List;

public abstract class Shape implements Cloneable {
	private AABB aabb = new AABB();
	
	private List<ShapeChangeListener> listeners = new LinkedList<>();
	
	
	/**
	 * an axis aligned bounding box that encloses the shape completely<br>
	 * will always return the exact same object
	 */
	public final AABB getAABB() {
		return aabb;
	}
	
	
	public final void addListener(ShapeChangeListener listener) {
		listeners.add(listener);
	}
	public final void removeListener(ShapeChangeListener listener) {
		listeners.add(listener);
	}
	
	
	protected final void callListeners() {
		for(ShapeChangeListener listener : listeners) {
			listener.shapeChanged(this);
		}
	}
	
	
	protected abstract void aabbChanged();
	
	public abstract Shape clone();
	
	
	public abstract void move(float x, float y);
	
	/**moves the shape so that the left side of it touches the given coordinate*/
	public abstract void setLeft(float left);
	/**moves the shape so that the right side of it touches the given coordinate*/
	public abstract void setRight(float right);
	/**moves the shape so that the bot side of it touches the given coordinate*/
	public abstract void setBot(float bot);
	/**moves the shape so that the top side of it touches the given coordinate*/
	public abstract void setTop(float top);
	
	
	public final boolean contains(float x, float y) {
		if(!aabbContains(x, y))
			return false;
		
		return shapeContains(x, y);
	}
	protected final boolean aabbContains(float x, float y) {
		return getAABB().contains(x, y);
	}
	protected boolean shapeContains(float x, float y) {
		return true;
	}
	
	
	public final boolean contains(Shape shape) {
		if(!aabbContains(shape))
			return false;
		
		return shapeContains(shape);
	}
	protected final boolean aabbContains(Shape shape) {
		return getAABB().contains(shape.getAABB());
	}
	protected boolean shapeContains(Shape shape) {
		return true;
	}
	
	
	public final boolean overlaps(Shape shape) {
		if(!aabbOverlaps(shape))
			return false;
		
		return shapeOverlaps(shape);
	}
	protected final boolean aabbOverlaps(Shape shape) {
		return getAABB().overlaps(shape.getAABB());
	}
	protected boolean shapeOverlaps(Shape shape) {
		return true;
	}
	
	public final boolean touches(Shape shape) {
		if(!aabbTouches(shape))
			return false;
		
		return shapeTouches(shape);
	}
	protected final boolean aabbTouches(Shape shape) {
		return getAABB().touches(shape.getAABB());
	}
	protected boolean shapeTouches(Shape shape) {
		return true;
	}
	
	
	public static interface ShapeChangeListener{
		public void shapeChanged(Shape shape);
	}
}

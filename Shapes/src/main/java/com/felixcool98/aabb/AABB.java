package com.felixcool98.aabb;

import java.util.LinkedList;
import java.util.List;

import com.felixcool98.math.VectorMath;

/**
 * Axis Aligned Bounding Box
 * 
 * @author felixcool98
 */
public class AABB {
	private float left, right;
	private float bot, top;
	
	private boolean overlapsWhileTouch = false;
	
	private List<AABBChangeListener> listeners = new LinkedList<AABBChangeListener>();
	
	
	public void enableOverlapsWhileTouching() {
		overlapsWhileTouch = true;
	}
	public void disableOverlapsWhileTouching() {
		overlapsWhileTouch = false;
	}
	
	
	public void setAll(float left, float bot, float right, float top) {
		if(this.left == left && this.bot == bot && this.right == right && this.top == top)
			return;
		
		this.left = left;
		this.bot = bot;
		this.right = right;
		this.top = top;
		
		callListeners();
	}
	
	
	public float getLeft() {
		return left;
	}
	public float getBot() {
		return bot;
	}
	public float getRight() {
		return right;
	}
	public float getTop() {
		return top;
	}
	
	public float getCenterX() {
		return getLeft() + (getRight()-getLeft())/2f;
	}
	public float getCenterY() {
		return getBot() + (getTop()-getBot())/2f;
	}
	
	public float getWidth() {
		return getRight()-getLeft();
	}
	public float getHeight() {
		return getTop()-getBot();
	}
	
	
	public boolean contains(AABB other) {
		return this.left <= other.left && this.right >= other.right &&
				this.bot <= other.bot && this.top >= other.top;
	}
	public boolean contains(float x, float y) {
		return this.left <= x && this.right >= x &&
				this.bot <= y && this.top >= y;
	}
	
	public boolean overlaps(AABB other) {
		if(this.left < other.right && this.right > other.left
				&& this.bot < other.top && this.top > other.bot)
			return true;
		
		if(overlapsWhileTouch || other.overlapsWhileTouch)
			return touches(other);
			
		return false;
	}
	
	public boolean touches(AABB other) {
		boolean leftEqOtherRight = this.left == other.right;
		boolean rightEqOtherLeft = this.right == other.left;
		boolean topEqOtherBot = this.top == other.bot;
		boolean botEqOtherTop = this.bot == other.top;
		
		boolean sameY = this.bot < other.top && this.top > other.bot;
		boolean sameX = this.left < other.right && this.right > other.left;
		
		boolean x = (leftEqOtherRight || rightEqOtherLeft) && sameY;
		boolean y = (topEqOtherBot || botEqOtherTop) && sameX;
		boolean sides = (topEqOtherBot || botEqOtherTop) && (rightEqOtherLeft || leftEqOtherRight);
		
		return x || y  || sides;
	}
	
	
	public float distanceTo(AABB other) {
		boolean left = other.getRight() < getLeft();
		boolean right = getRight() < other.getLeft();
		boolean bottom = other.getTop() < getBot();
		boolean top = getTop() < other.getBot();
		if(top && left)
			return VectorMath.vectorLength(getLeft(), getTop(), other.getRight(), other.getBot());
		else if(left && bottom)
			return VectorMath.vectorLength(getLeft(), getBot(), other.getRight(), other.getTop());
		else if(bottom && right)
			return VectorMath.vectorLength(getRight(), getBot(), other.getLeft(), other.getTop());
		else if(right && top)
			return VectorMath.vectorLength(getRight(), getTop(), other.getLeft(), other.getBot());
		else if(left)
			return getLeft() - other.getRight();
		else if(right)
			return other.getLeft() - getRight();
		else if(bottom)
			return getBot() - other.getTop();
		else if(top)
			return other.getBot() - getTop();
		else             // rectangles intersect
			return 0;
	}
	
	
	public void addListener(AABBChangeListener listener) {
		listeners.add(listener);
	}
	
	public void removeListener(AABBChangeListener listener) {
		listeners.remove(listener);
	}
	
	private void callListeners() {
		for(AABBChangeListener listener : listeners) {
			listener.changed(this);
		}
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof AABB))
			return false;
		
		AABB other = (AABB) obj;
		
		if(getLeft() != other.getLeft() || getRight() != other.getRight())
			return false;
		
		if(getTop() != other.getTop() || getBot() != other.getBot())
			return false;
		
		return true;
	}
		
	
	public static interface AABBChangeListener {
		public void changed(AABB aabb);
	}
}

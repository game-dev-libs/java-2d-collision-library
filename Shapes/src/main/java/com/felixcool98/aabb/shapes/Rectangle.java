package com.felixcool98.aabb.shapes;

import com.felixcool98.aabb.Shape;
import com.felixcool98.math.MathUtils;

public class Rectangle extends Shape {
	private float x, y;
	private float width, height;
	
	
	public Rectangle(Rectangle rectangle) {
		set(rectangle);
	}
	public Rectangle(float x, float y, float width, float height) {
		set(x, y, width, height);
	}
	public Rectangle(float width, float height) {
		setSize(width, height);
	}
	

	//======================================================================
	// getter
	//======================================================================
	
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
	
	public float getWidth() {
		return width;
	}
	public float getHeight() {
		return height;
	}
	
	public float getLeft() {
		return getX();
	}
	public float getBot() {
		return getY();
	}
	
	public float getRight() {
		return getX() + getWidth();
	}
	public float getTop() {
		return getY() + getHeight();
	}
	
	public float getCenterX() {
		return getLeft() + getWidth()/2f;
	}
	public float getCenterY() {
		return getBot() + getHeight()/2f;
	}
	

	//======================================================================
	// setter
	//======================================================================
	
	public void set(Rectangle rectangle) {
		set(rectangle.getX(), rectangle.getY(), rectangle.getWidth(), rectangle.getHeight());
	}
	
	public void set(float x, float y, float width, float height) {
		if(getX() == x && getY() == y && getWidth() == width && getHeight() == height)
			return;
		
		this.x = x;
		this.y = y;
		
		this.width = width;
		this.height = height;
		
		aabbChanged();
	}
	
	//position
	
	public void setX(float x) {
		if(getX() == x)
			return;
		
		this.x = x;
		
		aabbChanged();
	}
	public void setY(float y) {
		if(getY() == y)
			return;
		
		this.y = y;
		
		aabbChanged();
	}
	
	public void setPosition(float x, float y) {
		if(getX() == x && getY() == y)
			return;
		
		this.x = x;
		this.y = y;
		
		aabbChanged();
	}
	
	public void setCenter(float x, float y) {
		setPosition(x - getWidth()/2f, y - getHeight()/2f);
	}
	public void setCenterX(float x) {
		setX(x - getWidth()/2f);
	}
	public void setCenterY(float y) {
		setY(y - getHeight()/2f);
	}
	
	//size
	
	public void setWidth(float width) {
		if(getWidth() == width)
			return;
		
		this.width = width;
		
		aabbChanged();
	}
	public void setHeight(float height) {
		if(getHeight() == height)
			return;
		
		this.height = height;
		
		aabbChanged();
	}
	
	public void setSize(float width, float height) {
		if(getWidth() == width && getHeight() == height)
			return;
		
		this.width = width;
		this.height = height;
		
		aabbChanged();
	}
	
	
	//======================================================================
	// shape methods
	//======================================================================
	
	@Override
	public void move(float x, float y) {
		if(MathUtils.allZero(x, y))
			return;
		
		this.x += x;
		this.y += y;
		
		aabbChanged();
	}

	@Override
	public void setLeft(float left) {
		if(getLeft() == left)
			return;
		
		this.x = left;
		
		aabbChanged();
	}

	@Override
	public void setRight(float right) {
		if(getRight() == right)
			return;
		
		this.x = right - getWidth();
		
		aabbChanged();
	}

	@Override
	public void setBot(float bot) {
		if(getBot() == bot)
			return;
		
		this.y = bot;
		
		aabbChanged();
	}

	@Override
	public void setTop(float top) {
		if(getTop() == top)
			return;
		
		this.y = top - getHeight();
		
		aabbChanged();
	}

	@Override
	protected boolean shapeContains(float x, float y) {
		return this.getLeft() <= x && this.getRight() >= x &&
				this.getBot() <= y && this.getTop() >= y;
	}


	@Override
	protected void aabbChanged() {
		getAABB().setAll(getLeft(), getBot(), getRight(), getTop());
		
		callListeners();
	}
	
	
	//======================================================================
	// object overrides
	//======================================================================
	
	@Override
	public String toString() {
		return "Rectangle (x:"+getX()+" y:"+getY()+" w:"+getWidth()+" h:"+getHeight()+")";
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Rectangle))
			return false;
		
		Rectangle other = (Rectangle) obj;
		
		if(this.getX() != other.getX() || this.getY() != other.getY())
			return false;
		
		if(this.getWidth() != other.getWidth() || this.getHeight() != other.getHeight())
			return false;
		
		return true;
	}
	
	@Override
	public Rectangle clone() {
		return new Rectangle(this);
	}
}

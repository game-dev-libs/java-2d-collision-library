package com.felixcool98.aabb.shapes;

import com.felixcool98.aabb.Shape;
import com.felixcool98.math.MathUtils;

public class RelativeLine extends Shape {
	private float x, y;
	private float x2, y2;
	
	
	public RelativeLine(RelativeLine line) {
		getAABB().enableOverlapsWhileTouching();
		
		set(line);
	}
	public RelativeLine(float x, float y, float x2, float y2) {
		getAABB().enableOverlapsWhileTouching();
		
		set(x, y, x2, y2);
	}
	

	//======================================================================
	// getter
	//======================================================================
	
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
	
	public float getX2() {
		return x2;
	}
	public float getY2() {
		return y2;
	}
	
	public float getLeft() {
		return getX();
	}
	public float getBot() {
		return getY();
	}
	
	public float getRight() {
		return getX() + getX2();
	}
	public float getTop() {
		return getY() + getY2();
	}
	

	//======================================================================
	// setter
	//======================================================================
	
	public void set(RelativeLine line) {
		set(line.getX(), line.getY(), line.getX2(), line.getY2());
	}
	
	public void set(float x, float y, float x2, float y2) {
		if(getX() == x && getY() == y && getX2() == x2 && getY2() == y2)
			return;
		
		this.x = x;
		this.y = y;
		
		this.x2 = x2;
		this.y2 = y2;
		
		aabbChanged();
	}
	
	public void setX(float x) {
		if(getX() == x)
			return;
		
		this.x = x;
		
		aabbChanged();
	}
	public void setY(float y) {
		if(getY() == y)
			return;
		
		this.y = y;
		
		aabbChanged();
	}
	
	public void setX2(float x2) {
		if(getX2() == x2)
			return;
		
		this.x2 = x2;
		
		aabbChanged();
	}
	public void setY2(float y2) {
		if(getY2() == y2)
			return;
		
		this.y2 = y2;
		
		aabbChanged();
	}
	
	public void setPosition(float x, float y) {
		if(getX() == x && getY() == y)
			return;
		
		this.x = x;
		this.y = y;
		
		aabbChanged();
	}
	public void setRelativePosition(float x2, float y2) {
		if(getX2() == x2 && getY2() == y2)
			return;
		
		this.x2 = x2;
		this.y2 = y2;
		
		aabbChanged();
	}
	
	
	
	//======================================================================
	// shape methods
	//======================================================================
	
	@Override
	public void move(float x, float y) {
		if(MathUtils.allZero(x, y))
			return;
		
		this.x += x;
		this.y += y;
		
		aabbChanged();
	}

	@Override
	public void setLeft(float left) {
		if(getLeft() == left)
			return;
		
		this.x = left;
		
		aabbChanged();
	}

	@Override
	public void setRight(float right) {
		setX(right - getX2());
	}

	@Override
	public void setBot(float bot) {
		if(getBot() == bot)
			return;
		
		this.y = bot;
		
		aabbChanged();
	}

	@Override
	public void setTop(float top) {
		setY(top - getY2());
	}

	@Override
	protected boolean shapeContains(float x, float y) {
		if(getX2() == 0 && getY2() == 0) 
			return getX() == x && getY() == y;
		
		
		float dxc = x - getX();
		float dyc = y - getY();

		float dxl = getX()+getX2() - getX();
		float dyl = getY()+getY2() - getY();

		float cross = dxc * dyl - dyc * dxl;
		
		if (cross != 0)
			  return false;
		
		if (Math.abs(dxl) >= Math.abs(dyl))
			  return dxl > 0 ? 
			    getX() <= x && x <= getX()+getX2() :
			    getX()+getX2() <= x && x <= getX();
			else
			  return dyl > 0 ? 
			    getY() <= y && y <= getY()+getY2() :
			    getY()+getY2() <= y && y <= getY();
	}


	@Override
	protected void aabbChanged() {
		getAABB().setAll(getLeft(), getBot(), getRight(), getTop());
		
		callListeners();
	}
	
	
	//======================================================================
	// object overrides
	//======================================================================
	
	@Override
	public String toString() {
		return "Relative Line (x:"+getX()+" y:"+getY()+" x2:"+getX2()+" y2:"+getY2()+")";
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof RelativeLine))
			return false;
		
		RelativeLine other = (RelativeLine) obj;
		
		if(this.getX() != other.getX() || this.getY() != other.getY())
			return false;
		
		if(this.getX2() != other.getX2() || this.getY2() != other.getY2())
			return false;
		
		return true;
	}
	
	@Override
	public RelativeLine clone() {
		return new RelativeLine(this);
	}
}

package com.felixcool98.aabb.shapes;

import com.felixcool98.aabb.Shape;
import com.felixcool98.math.VectorMath;

public class Circle extends Shape {
	private float x, y;
	private float radius;
	
	
	public Circle(Circle circle) {
		set(circle);
	}
	public Circle(float radius) {
		this(0, 0, radius);
	}
	public Circle(float x, float y, float radius) {
		set(x, y, radius);
	}
	
	
	//======================================================================
	// getter
	//======================================================================
	
	public float getX() {
		return x;
	}
	public float getY(){
		return y;
	}
	
	public float getRadius() {
		return radius;
	}
	
	public float getLeft() {
		return getX() - getRadius();
	}
	public float getBot() {
		return getY() - getRadius();
	}
	
	public float getRight() {
		return getX() + getRadius();
	}
	public float getTop() {
		return getY() + getRadius();
	}
	
	
	//======================================================================
	// setter
	//======================================================================
	
	public void setX(float x) {
		if(getX() == x)
			return;
		
		this.x = x;
		
		aabbChanged();
	}
	public void setY(float y) {
		if(getY() == y)
			return;
		
		this.y = y;
		
		aabbChanged();
	}
	public void setPosition(float x, float y) {
		if(getX() == x && getY() == y)
			return;
		
		this.x = x;
		this.y = y;
		
		aabbChanged();
	}
	public void setRadius(float radius) {
		if(getRadius() == radius)
			return;
		
		this.radius = radius;
		
		aabbChanged();
	}
	public void set(float x, float y, float radius) {
		if(getX() == x && getY() == y && getRadius() == radius)
			return;
		
		this.x = x;
		this.y = y;
		
		this.radius = radius;
		
		aabbChanged();
	}
	public void set(Circle circle) {
		set(circle.getX(), circle.getY(), circle.getRadius());
	}
	
	
	//======================================================================
	// shape methods
	//======================================================================
	
	@Override
	public void move(float x, float y) {
		setPosition(getX()+x, getY()+y);
	}

	@Override
	public void setLeft(float left) {
		setX(left + getRadius());
	}
	@Override
	public void setBot(float bot) {
		setY(bot + getRadius());
	}

	@Override
	public void setRight(float right) {
		setX(right - getRadius());
	}
	@Override
	public void setTop(float top) {
		setY(top - getRadius());
	}

	@Override
	protected boolean shapeContains(float x, float y) {
		return VectorMath.inRange(getX(), getY(), x, y, getRadius());
	}

	@Override
	protected void aabbChanged() {
		getAABB().setAll(getLeft(), getBot(), getRight(), getTop());
		
		callListeners();
	}

	
	//======================================================================
	// object overrides
	//======================================================================
	
	@Override
	public Circle clone() {
		return new Circle(this);
	}

}

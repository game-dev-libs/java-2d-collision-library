package shapes.aabb;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.aabb.AABB;

public class AABBTests {

	@Test
	public void test() {
		AABB aabb = new AABB();
		aabb.setAll(0, 0, 1, 1);
		
		AABB other = new AABB();
		
		//left
		other.setAll(-2, 0, -1, 1);
		assertEquals(1, aabb.distanceTo(other), 0.0001f);
		
		//right
		other.setAll(2, 0, 3, 1);
		assertEquals(1, aabb.distanceTo(other), 0.0001f);
		
		//bot
		other.setAll(0, -2, 1, -1);
		assertEquals(1, aabb.distanceTo(other), 0.0001f);
		
		//top
		other.setAll(0, 2, 1, 3);
		assertEquals(1, aabb.distanceTo(other), 0.0001f);
	}

}

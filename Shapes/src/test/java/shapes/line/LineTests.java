package shapes.line;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.aabb.shapes.RelativeLine;

public class LineTests {

	@Test
	public void test() {
		RelativeLine line = new RelativeLine(10, 5, 2, 3);
		
		assertTrue(line.contains(10, 5));
		assertTrue(line.contains(12, 8));
		assertTrue(line.contains(11, 6.5f));
	}

}

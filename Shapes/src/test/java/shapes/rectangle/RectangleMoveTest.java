package shapes.rectangle;

import static org.junit.Assert.*;

import org.junit.Test;

import com.felixcool98.aabb.shapes.Rectangle;

public class RectangleMoveTest {

	@Test
	public void test() {
		Rectangle rect = new Rectangle(1, 1);
		
		rect.move(1, 0);
		
		assertEquals(1, rect.getX(), 0.1f);
		assertEquals(0, rect.getY(), 0.1f);
		
		rect.move(0, 1);
		
		assertEquals(1, rect.getX(), 0.1f);
		assertEquals(1, rect.getY(), 0.1f);
		
		rect.move(1, 1);
		
		assertEquals(2, rect.getX(), 0.1f);
		assertEquals(2, rect.getY(), 0.1f);
		
		rect.move(-1, -1);
		
		assertEquals(1, rect.getX(), 0.1f);
		assertEquals(1, rect.getY(), 0.1f);
		
		rect.move(1, -1);
		
		assertEquals(2, rect.getX(), 0.1f);
		assertEquals(0, rect.getY(), 0.1f);
	}

}
